import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SignInPage {

    private static final String MAIN_PAGE_URL ="http://automationpractice.com";
    private WebDriver driver;

    private By buttonSignIn = By.xpath("//a[@title='Log in to your customer account']");
    private By fieldEmailCreateAcc = By.xpath("//input[@name='email_create']");
    private By buttonCreateAcc = By.xpath("//button[@id='SubmitCreate']");
    private By errorMessage = By.xpath("//div[@class='alert alert-danger']//p");

    public SignInPage(WebDriver driver) {
        this.driver = driver;
    }

    public SignInPage openMainPage(){
        this.driver.navigate().to(MAIN_PAGE_URL);
        return this;
    }

    public SignInPage openSignInPage(){
        this.driver.findElement(buttonSignIn).click();
        return this;
    }

    public SignInPage editEmail()  {
        this.driver.findElement(fieldEmailCreateAcc).sendKeys("test789555@mail.com");
        return this;
    }

    public SignInPage pressCreateAccount() throws InterruptedException {
        this.driver.findElement(buttonCreateAcc).click();
        Thread.sleep(5*1000);
        return this;
    }

    public SignInPage editFields(){
        this.driver.findElement(By.xpath("//input[@name='customer_firstname']")).sendKeys("Vasya");
        this.driver.findElement(By.xpath("//input[@name='customer_lastname']")).sendKeys("Pupkin");
        this.driver.findElement(By.xpath("//input[@name='passwd']")).sendKeys("par0l");
        this.driver.findElement(By.xpath("//input[@name='address1']")).sendKeys("ulica");
        this.driver.findElement(By.xpath("//input[@name='city']")).sendKeys("Gorod");
        this.driver.findElement(By.xpath("//input[@name='postcode']")).sendKeys("12345");
        this.driver.findElement(By.xpath("//input[@name='phone_mobile']")).sendKeys("+380991234455");
        this.driver.findElement(By.xpath("//input[@name='alias']")).sendKeys("Rabochiy adress");
        return this;
    }

    public SignInPage pressRegister() throws InterruptedException {
        this.driver.findElement(By.xpath("//button[@id='submitAccount']")).click();
        Thread.sleep(5*1000);
        return this;
    }

    public boolean checkError(){
        String message = this.driver.findElement(errorMessage).getText();
        return message.contains("error")?true:false;
    }


}